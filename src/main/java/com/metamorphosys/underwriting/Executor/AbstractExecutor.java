package com.metamorphosys.underwriting.Executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.jpa.transaction.CaseRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.underwriting.dataobjects.common.ResultDO;
import com.metamorphosys.underwriting.interfaceobjects.BaseIO;
import com.metamorphosys.underwriting.interfaceobjects.CreateCaseIO;
import com.metamorphosys.underwriting.utility.SerializationUtility;

public abstract class AbstractExecutor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExecutor.class);
	
	@Autowired
	CaseRepository caseRepository;

	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired
	WebGetController webGetController;
	
	public CreateCaseIO init(CreateCaseIO io) {
		io = callRuleEngine(io,"EXECUTE");
		return io;
		
	}

	public CreateCaseIO execute(CreateCaseIO io) {
		io = callRuleEngine(io,"EXECUTE");
		caseRepository.save((CaseDO)io.getCaseDO());
		return io;
	}
	
	public CreateCaseIO load(CreateCaseIO io) {
		
		CaseDO quotationDO = caseRepository.findById( io.getCaseDO().getId());
		io.setCaseDO(quotationDO);
		return io;
	}

	private CreateCaseIO callRuleEngine(CreateCaseIO io,String method) {

		String tojson = covertTojson(io);
		LOGGER.info(tojson);
		ResponseEntity<String> jsonObject=null;
		if(InsureConnectConstants.Method.EXECUTE.equals(method)){
			jsonObject = webExecuteController.executeJson(tojson);
		}else if(InsureConnectConstants.Method.GET.equals(method)){
			jsonObject = webGetController.execute(tojson);
		}
		/*RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(tojson.toString(), httpHeaders);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url,
				HttpMethod.POST, entity, String.class);
*/		io = convertjsonToObject(jsonObject.getBody(), io);

		return io;
	}

	private CreateCaseIO convertjsonToObject(String json, CreateCaseIO io) {

		String data = null;
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(json);

		if (null != map && map.containsKey("data")) {
			data = map.get("data").toString();
		}
		
		ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) SerializationUtility.getInstance().fromJson(data, ArrayList.class);
		if (null != responseDataList) {
			for (LinkedTreeMap responseDataIO : responseDataList) {
				if (InsureConnectConstants.DataObjects.CASEDO.equals(responseDataIO.get("objectName").toString())) {

					CaseDO baseDO = (CaseDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), CaseDO.class);
					io.setCaseDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					ServiceDO baseDO = (ServiceDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ServiceDO.class);

					io.setServiceDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.RESULTDO.equals(responseDataIO.get("objectName").toString())) {
					ResultDO baseDO = (ResultDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ResultDO.class);

					io.setResultDO(baseDO);
				}

			}
		}
		return io;
	}

	public String covertTojson(CreateCaseIO io) {
		List<BaseIO> baseIOs = new ArrayList<BaseIO>();

		BaseIO baseIO = new BaseIO();
		baseIO.setObjectName(io.getCaseDO().getObjectName());
		baseIO.setObject(SerializationUtility.getInstance().toJson(io.getCaseDO()));
		baseIOs.add(baseIO);

		if(io.getServiceDO()!=null){
			BaseIO baseIO1 = new BaseIO();
			baseIO1.setObjectName(io.getServiceDO().getObjectName());
			baseIO1.setObject(SerializationUtility.getInstance().toJson(io.getServiceDO()));
			baseIOs.add(baseIO1);
		}

		String serialize = SerializationUtility.getInstance().toJson(baseIOs);
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("data", serialize);
		String finalString = SerializationUtility.getInstance().toJson(dataMap);
		

		return finalString;

	}

	public abstract ServiceDO getService();

}
