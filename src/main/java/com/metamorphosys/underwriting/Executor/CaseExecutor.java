package com.metamorphosys.underwriting.Executor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.utilities.SequenceGeneratorUtil;
import com.metamorphosys.underwriting.interfaceobjects.CreateCaseIO;
import com.metamorphosys.underwriting.utility.GuidGeneratorUtility;

@Component
public class CaseExecutor extends AbstractExecutor {
	
	@Autowired
	SequenceGeneratorUtil sequenceGeneratorUtil;

	@Override
	public CreateCaseIO init(CreateCaseIO io) {
		if(io.getServiceDO()==null){
			io.setServiceDO(getService());
		}
		super.init(io);
		return io;
	}
	
	@Override
	public ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("CREATCASE");
		serviceDO.setServiceType("WORKFLOW");
		serviceDO.setGuid(GuidGeneratorUtility.getInstance().generateGuid());
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		return serviceDO;
	}
	
}
