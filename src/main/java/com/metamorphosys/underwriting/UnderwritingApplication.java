package com.metamorphosys.underwriting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages={"com.metamorphosys"})
public class UnderwritingApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(UnderwritingApplication.class, args);
	}
}
