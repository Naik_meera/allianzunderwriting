package com.metamorphosys.underwriting.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;
import com.metamorphosys.underwriting.Executor.CaseExecutor;
import com.metamorphosys.underwriting.interfaceobjects.CreateCaseIO;

@RestController
@RequestMapping("/api/case")
public class CaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CaseController.class);

	@Autowired
	CaseExecutor caseExecutor;
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity init(){
		LOGGER.info("init CreateQuoteController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		CreateCaseIO io = new CreateCaseIO();
		CaseDO caseDO= new CaseDO();
		io.setCaseDO(caseDO);
		io=caseExecutor.init(io);
		return new ResponseEntity(io, httpHeaders, httpStatus);		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid CreateCaseIO io){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save CreateQuoteController");
		io=caseExecutor.execute(io);
		return new ResponseEntity(io, httpHeaders, httpStatus);			
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("save PolicyController");
		
		CaseDO caseDO = new CaseDO();
		caseDO.setId(id);
		CreateCaseIO io = new CreateCaseIO();
		io.setCaseDO(caseDO);
		io=caseExecutor.load(io);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);	
		
	}

}
