package com.metamorphosys.underwriting.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;
import com.metamorphosys.underwriting.Executor.CaseExecutor;
import com.metamorphosys.underwriting.interfaceobjects.CreateCaseIO;
import com.metamorphosys.underwriting.utility.CaseConversionUtil;
import com.metamorphosys.underwriting.utility.SerializationUtility;

@RestController
@RequestMapping("/api/copyCase")
final class DemoCopyCaseController {

	@Autowired
	CaseExecutor caseExecutor;

	private static final Logger LOGGER =  LoggerFactory.getLogger(DemoCopyCaseController.class);
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity executeJson (@RequestBody String json) {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		
		CaseDO caseDO = (CaseDO) SerializationUtility.getInstance().fromJson(json, CaseDO.class);
		CreateCaseIO io = new CreateCaseIO();
		io.setCaseDO(caseDO);
		
		//LOGGER.info("Init TransferPolicyController");
		io=  caseExecutor.init(io);
		
		//LOGGER.info("Init TransferPolicyController");
		io = caseExecutor.execute(io);
		
		CaseConversionUtil.getInstance().convertionUtil(caseDO);
		
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("Rresult", "SUCCESS");
		String json1 = SerializationUtility.getInstance().toJson(dataMap);
		return new ResponseEntity(json1, httpHeaders, httpStatus);	
	}
}
