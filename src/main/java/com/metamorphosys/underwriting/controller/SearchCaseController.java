package com.metamorphosys.underwriting.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;
import com.metamorphosys.insureconnect.jpa.transaction.CaseRepository;

@RestController
@RequestMapping("/api/searchCase")
public class SearchCaseController {

		private static final Logger LOGGER =  LoggerFactory.getLogger(SearchCaseController.class);
		
		@Autowired
		CaseRepository caseRepository;
		
		@RequestMapping(method = RequestMethod.GET)
		ResponseEntity fetch() {
			
			HttpStatus httpStatus = HttpStatus.OK;
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setAccessControlAllowOrigin("*");
			httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
			
			LOGGER.info("Fetch SearchCaseController");
			
			List<CaseDO> caseList =(List<CaseDO>) caseRepository.findAll();
			
			return new ResponseEntity(caseList, httpHeaders, httpStatus);	
		}
	
}
