package com.metamorphosys.underwriting.dataobjects;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "uwEntityManagerFactory",
		transactionManagerRef = "uwTransactionManager", basePackages={"com.metamorphosys.underwriting.repository"})
public class UnderwritingConfig {

	@Bean
	PlatformTransactionManager uwTransactionManager() {
		return new JpaTransactionManager(uwEntityManagerFactory().getObject());
	}

	@Bean
	LocalContainerEntityManagerFactoryBean uwEntityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setDatabase(Database.ORACLE);
		//vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(underwritingDataSource());
		factoryBean.setJpaVendorAdapter(vendorAdapter);
		factoryBean.setPackagesToScan(this.getClass().getPackage().getName());

		return factoryBean;
	}

	
	@Bean
	@ConfigurationProperties(prefix="primary.spring.datasource")
	public DataSource underwritingDataSource() {
	    return DataSourceBuilder.create().build();
	}

}
