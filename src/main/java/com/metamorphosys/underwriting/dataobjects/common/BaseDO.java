package com.metamorphosys.underwriting.dataobjects.common;

import java.sql.Timestamp;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseDO {
	
	public Integer versionId;
	
	public String guid;
	
	Timestamp systemCreatedDt;
	
	Timestamp systemUpdatedDt;
	
	public String systemCreatedUser;
	
	public String systemUpdatedUser;

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Timestamp getSystemCreatedDt() {
		return systemCreatedDt;
	}

	public void setSystemCreatedDt(Timestamp systemCreatedDt) {
		this.systemCreatedDt = systemCreatedDt;
	}

	public Timestamp getSystemUpdatedDt() {
		return systemUpdatedDt;
	}

	public void setSystemUpdatedDt(Timestamp systemUpdatedDt) {
		this.systemUpdatedDt = systemUpdatedDt;
	}

	public String getSystemCreatedUser() {
		return systemCreatedUser;
	}

	public void setSystemCreatedUser(String systemCreatedUser) {
		this.systemCreatedUser = systemCreatedUser;
	}

	public String getSystemUpdatedUser() {
		return systemUpdatedUser;
	}

	public void setSystemUpdatedUser(String systemUpdatedUser) {
		this.systemUpdatedUser = systemUpdatedUser;
	}
	
	public abstract Long getId();
	
	public abstract String getObjectName();
}
