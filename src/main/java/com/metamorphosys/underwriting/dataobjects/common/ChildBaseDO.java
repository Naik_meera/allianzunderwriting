package com.metamorphosys.underwriting.dataobjects.common;

public abstract class ChildBaseDO {
	
	private BaseDO parentObject;

	public BaseDO getParentObject() {
		return parentObject;
	}

	public void setParentObject(BaseDO parentObject) {
		this.parentObject = parentObject;
	}

	public abstract Long getId();
	
}
