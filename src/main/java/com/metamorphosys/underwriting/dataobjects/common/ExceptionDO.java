package com.metamorphosys.underwriting.dataobjects.common;

import java.util.HashMap;

public class ExceptionDO extends BaseDO {

	private String exceptionCd;
	private String exceptionMessage;
	
	private String referenceObject;
	private String referenceAttribute;
	
	
		
	public String getExceptionCd() {
		return exceptionCd;
	}

	public void setExceptionCd(String exceptionCd) {
		this.exceptionCd = exceptionCd;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getReferenceObject() {
		return referenceObject;
	}

	public void setReferenceObject(String referenceObject) {
		this.referenceObject = referenceObject;
	}

	public String getReferenceAttribute() {
		return referenceAttribute;
	}

	public void setReferenceAttribute(String referenceAttribute) {
		this.referenceAttribute = referenceAttribute;
	}

	@Override
	public String getObjectName() {		
		return ExceptionDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
