package com.metamorphosys.underwriting.dataobjects.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResultDO extends BaseDO {

	private HashMap<String, Object> resultMap = new HashMap<String, Object>();
	
	private List<ExceptionDO> exceptionList = new ArrayList<ExceptionDO>();
	
	
	public HashMap<String, Object> getResultMap() {
		return resultMap;
	}

	public void setResultMap(HashMap<String, Object> resultMap) {
		this.resultMap = resultMap;
	}

	public List<ExceptionDO> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(List<ExceptionDO> exceptionList) {
		this.exceptionList = exceptionList;
	}

	@Override
	public String getObjectName() {		
		return ResultDO.class.getSimpleName();
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
