package com.metamorphosys.underwriting.interfaceobjects;

import com.metamorphosys.underwriting.dataobjects.common.BaseDO;

public class BaseIO {
	
private String objectName;
	
	private String object;

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

}
