package com.metamorphosys.underwriting.interfaceobjects;

import java.util.List;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.underwriting.dataobjects.common.ResultDO;

public class CreateCaseIO {

	public  CaseDO caseDO;
	
	public List<CaseDO> caseDOList;

	public ServiceDO serviceDO;
	
	public ResultDO resultDO;

		public CaseDO getCaseDO() {
		return caseDO;
	}

	public void setCaseDO(CaseDO caseDO) {
		this.caseDO = caseDO;
	}

	public List<CaseDO> getCaseDOList() {
		return caseDOList;
	}

	public void setCaseDOList(List<CaseDO> caseDOList) {
		this.caseDOList = caseDOList;
	}

	public ResultDO getResultDO() {
		return resultDO;
	}

	public void setResultDO(ResultDO resultDO) {
		this.resultDO = resultDO;
	}

		public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}
	
}
