package com.metamorphosys.underwriting.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.metamorphosys.insureconnect.dataobjects.transaction.CaseClientDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.CaseCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.CaseDO;

public class CaseConversionUtil {
	
	private static CaseConversionUtil utility;
  
	public static CaseConversionUtil getInstance()
	{
		if(null == utility)
		{
			utility = new CaseConversionUtil();
		}
		
		return utility;
	}
	
	public void convertionUtil(CaseDO caseDO){
      Connection c = null;
      PreparedStatement preparedStatement = null;
      
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/mydb",
            "postgres", "root");
         c.setAutoCommit(false);

         
         String caseQuery = "INSERT INTO case_master(\"Policy_No\", \"Application Date\", \"Policy Type\", \"Status\", \"Product\", \"UW Decision\")"
         		+ " VALUES (?, ?, ?, ?, ?, ?);";
         
         preparedStatement = c.prepareStatement(caseQuery);
         preparedStatement.setString(1, caseDO.getPolicyNum());
         preparedStatement.setString(2,  "21/07/2016");
         preparedStatement.setString(3, caseDO.getPolicyTypeCd());
         preparedStatement.setString(4, caseDO.getPolicyStatusCd());
         preparedStatement.setString(5, caseDO.getBaseProductCd());
         preparedStatement.setString(6, caseDO.getUwDecisionCd());
         preparedStatement.executeUpdate();
         
         for(CaseClientDO clientDO : caseDO.getCaseClientList()){
        	 PreparedStatement preparedStatement2 = null;
        	 if("PROPOSER".equals(clientDO.getRoleCd())){
		         String policyHolder = "INSERT INTO case_insured(\"Insured\",\"Gender\",\"Policy_No\") VALUES (?, ?, ?);";
		         
		         preparedStatement2 = c.prepareStatement(policyHolder);
		         preparedStatement2.setString(1, clientDO.getFirstName());
		         preparedStatement2.setString(2, clientDO.getGenderCd());
		         //preparedStatement2.setString(3, clientDO.getDateOfBirth().toString());
		         preparedStatement2.setString(3, caseDO.getPolicyNum());
		  
		         preparedStatement2.execute();
        	 }
         }
		         
		  for(CaseCoverageDO coverageDO:caseDO.getCaseCoverageList()){
    		 PreparedStatement preparedStatement4 = null;
    		 String vehicleDetails="INSERT INTO public.case_coverage(\"Coverage\", \"Coverage_Type\", \"Benefit\", \"Base_Premium\", \"Extra_Premium\","
    		 		+ " \"Policy_No\") VALUES (?, ?, ?, ?, ?, ?);";
    		 
    		 preparedStatement4 = c.prepareStatement(vehicleDetails);
    		 preparedStatement4.setString(1, coverageDO.getProductCd());
    		 preparedStatement4.setString(2, coverageDO.getCoverageType());
    		 if(coverageDO.getSumInsured()!=null){
    			 preparedStatement4.setDouble(3, coverageDO.getSumInsured());
    		 }else{
    			 preparedStatement4.setDouble(3, 0);
    		 }
    		 if(coverageDO.getBaseAnnualPremium()!=null){
    			 preparedStatement4.setDouble(4, coverageDO.getBaseAnnualPremium());
    		 }else{
    			 preparedStatement4.setDouble(4, 0);
    		 }
	         if(coverageDO.getTotalExtraPremium()==null){
	        	 preparedStatement4.setDouble(5, 0);
	         }else{
	        	 preparedStatement4.setDouble(5, coverageDO.getTotalExtraPremium()); 
	         }
	         preparedStatement4.setString(6, caseDO.getPolicyNum());
	         
    		 preparedStatement4.execute();
         }
         
         c.commit();
         c.close();
      } catch (Exception e) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
   }
}