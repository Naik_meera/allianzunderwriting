package com.metamorphosys.underwriting.utility;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateAdapter implements JsonDeserializer<Date>, JsonSerializer<Date> {

	private static final Logger log = LoggerFactory.getLogger(DateAdapter.class);
	private DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	private DateFormat timeFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	
	public JsonElement serialize(Date date, Type type, JsonSerializationContext context) 
	{	
		JsonPrimitive returnValue = null;
		
		try
		{
			if(null != date)
			{
				//returnValue = new JsonPrimitive(timeFormatter.format(date));
				returnValue=new JsonPrimitive(date.getTime());
			}
			else
			{
				log.error("date is null!!!");
			}
		}
		catch(Exception e)
		{
			log.error("Error ["+date+"]: "+e.getMessage());
		}
		
		return returnValue;
	}

//	public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException 
//	{	
//		Date returnValue = null;
//		Date date = null;
//		try
//		{
//			if(!"".equals(json.toString()))
//			{
//				// it's hack and not a right way to do it. Need to refactor the unix epoc to date conversion ASAP
//				if(json.toString().length() == 5 || json.toString().length() == 6 || json.toString().length() == 7 || json.toString().length() == 8 || json.toString().length() == 9 || json.toString().length() == 10 || json.toString().length() == 11 || json.toString().length() == 13){
//					date=new Date(json.getAsLong());
//				}
//				if(json.toString().length() == 10)
//				{
//					date = dateFormatter.parse(json.toString());			    
//				}
//				else if(json.toString().length() == 12)
//				{
////					date = dateFormatter.parse(json.toString().replace("\"", ""));			
//					date=new Date(json.getAsLong());
//				}
//				else if(json.toString().length() == 19)
//				{
//					date = timeFormatter.parse(json.toString());			    
//				}
//				else if(json.toString().length() == 21)
//				{
//					date = timeFormatter.parse(json.toString().replace("\"", ""));			    
//				}
//				else
//				{
//					log.error("Unknown date format: ["+json.toString()+"]");
//				}
//				
//				if(null != date)
//				{
//					/*switch(type.toString())
//					{
//					case "class java.util.Date":
//						returnValue = date;
//						break;
//					case "class java.sql.Date":
//						returnValue = new java.sql.Date(date.getTime());
//						break;
//					case "class java.sql.Timestamp":
//						returnValue = new Timestamp(date.getTime());
//						break;
//					default:
//						returnValue = new Timestamp(date.getTime());
//						break;
//					}*/
//					if( "class java.util.Date".equals(type.toString())){
//						returnValue = date;
//					}else if( "class java.sql.Date".equals(type.toString())){
//						returnValue = new java.sql.Date(date.getTime());
//					}else if( "class java.sql.Timestamp".equals(type.toString())){
//						returnValue = new Timestamp(date.getTime());
//					}else{
//						returnValue = new Timestamp(date.getTime());
//					}
//				}
//			}
//		}
//		catch(Exception e)
//		{
//			log.error("Cannot convert ["+type.getTypeName()+"] to number: "+json.toString());
//		}
//        
//        return returnValue;
//	}
	
	public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException 
	{	
		log.info("Date is :"+json.getAsString());
		Date date = null;
		Long dateInJson = Long.valueOf(json.getAsString());
		try
		{
			if(!"".equals(json.toString()))
			{
					if( "class java.util.Date".equals(type.toString())){
						log.info("I am returning java.util.Date while deserializing");
						date=new java.util.Date(dateInJson);
					}else if( "class java.sql.Date".equals(type.toString())){
						log.error("return java.sql.Date");
						date = new java.sql.Date(dateInJson);
					}else if( "class java.sql.Timestamp".equals(type.toString())){
						log.error("return java.sql.Timestamp");
						date = new Timestamp(dateInJson);
					}else{
						log.error("return Timestamp");
						date = new Timestamp(dateInJson);
					}
			}
			log.info("Deserialized Date:"+date.toString());
		}
		catch(Exception e)
		{
			log.error("Cannot convert ["+type.getTypeName()+"] to number: "+json.toString());
		}
        
        return date;
	}
}
