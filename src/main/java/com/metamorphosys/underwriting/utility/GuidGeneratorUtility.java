package com.metamorphosys.underwriting.utility;

import java.util.UUID;

public class GuidGeneratorUtility {
	
	private static GuidGeneratorUtility utility;

	public static GuidGeneratorUtility getInstance()
	{
		if(null == utility)
		{
			utility = new GuidGeneratorUtility();
		}
		
		return utility;
	}
	
	public String generateGuid(){
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
		return randomUUIDString;		
	}
}
