package com.metamorphosys.underwriting.utility;

import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class NumberAdapter implements JsonDeserializer<Number>, JsonSerializer<Number> {

	private static final Logger log = LoggerFactory.getLogger(NumberAdapter.class);
	
	public JsonElement serialize(Number arg0, Type arg1, JsonSerializationContext arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	public Number deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException 
	{	
		Number returnValue = null;
		try
		{
			/*switch(type.toString())
			{
			case "class java.lang.Short":
				returnValue = Short.parseShort(json.getAsString());
				break;
			case "class java.lang.Integer":
				returnValue = Integer.parseInt(json.getAsString());
				break;
			case "class java.lang.Long":
				returnValue = Long.parseLong(json.getAsString());
				break;
			case "class java.lang.Float":
				returnValue = Float.parseFloat(json.getAsString());
				break;
			case "class java.lang.Double":
				returnValue = Double.parseDouble(json.getAsString());
				break;
			default:
				log.error("Unsupported object type ["+type.getTypeName()+"] ");
				returnValue = json.getAsNumber();
			}*/
		if( "class java.lang.Short".equals(type.toString())){
			returnValue = Short.parseShort(json.getAsString());
		}else
		if("class java.lang.Integer".equals(type.toString())){
			returnValue = Integer.parseInt(json.getAsString());
		}
		if( "class java.lang.Long".equals(type.toString())){
			returnValue = Long.parseLong(json.getAsString());
		}
		if( "class java.lang.Float".equals(type.toString())){
			returnValue = Float.parseFloat(json.getAsString());
		}
		if( "class java.lang.Double".equals(type.toString())){
			returnValue = Double.parseDouble(json.getAsString());
		}else{
			log.error("Unsupported object type ["+type.getTypeName()+"] ");
			returnValue = json.getAsNumber();
		}
		
		}catch(Exception e)
		{
			log.error("Cannot convert ["+type.getTypeName()+"] to number: "+json.toString());
		}
        
        return returnValue;
	}
}
