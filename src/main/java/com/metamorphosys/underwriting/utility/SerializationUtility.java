package com.metamorphosys.underwriting.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

public class SerializationUtility {

	private static SerializationUtility utility;
	
	private Gson serializer;
	private Gson deserializer;
	
	private SerializationUtility()
	{
		serializer = new GsonBuilder()
				.registerTypeAdapter(java.util.Date.class, new DateAdapter())
				.registerTypeAdapter(java.sql.Date.class, new DateAdapter())
				.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter())
				.create();

deserializer = new GsonBuilder()
				.registerTypeAdapter(java.util.Date.class, new DateAdapter())
				.registerTypeAdapter(java.sql.Date.class, new DateAdapter())
				.registerTypeAdapter(java.sql.Timestamp.class, new DateAdapter())
				.registerTypeAdapter(Short.class, new NumberAdapter())
				.registerTypeAdapter(Integer.class, new NumberAdapter())
				.registerTypeAdapter(Long.class, new NumberAdapter())
				.registerTypeAdapter(Float.class, new NumberAdapter())
				.registerTypeAdapter(Double.class, new NumberAdapter())
				.create();
	}
	
	public static SerializationUtility getInstance()
	{
		if(null == utility)
		{
			utility = new SerializationUtility();
		}
		
		return utility;
	}
	
	public String toJson(Object object)
	{
		return this.serializer.toJson(object);
	}

	public LinkedTreeMap fromJson(String json)
	{
		LinkedTreeMap returnObject = this.deserializer.fromJson(json, LinkedTreeMap.class);
		
		return returnObject;
	}

	public Object fromJson(String json, Class objectClass)
	{
		Object returnObject = this.deserializer.fromJson(json, objectClass);
		
		return returnObject;
	}

}
