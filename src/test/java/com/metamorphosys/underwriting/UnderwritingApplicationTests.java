package com.metamorphosys.underwriting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.metamorphosys.underwriting.UnderwritingApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UnderwritingApplication.class)
@WebAppConfiguration
public class UnderwritingApplicationTests {

	@Test
	public void contextLoads() {
	}

}
